//#3
db.collections.insertOne({
    "name": "single",
    "accommodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
})



//#4
db.collections.insertMany ([
    {"name": "double", 
     "accommodates": 3,
     "price": 2000, 
     "description": "A room fit for a small family going on a vacation",
     "rooms_available": 5,
     "isAvailable": false },
     {"name": "queen", 
     "accommodates": 4,
     "price": 4000, 
     "description": "A room with a queen sized bed perfect for a simple gateway",
     "rooms_available": 15,
     "isAvailable": false}
])
db.collections.find()
////5
db.collections.find({"name": "double"})

//6
db.collections.updateOne({"_id":ObjectId("6419b5590d38ce5588eb0c41")},
{$set: {"rooms_available": 0}})
db.collections.find()

db.collections.updateOne({"_id": ObjectId("6419b55e0d38ce5588eb0c44")},
{$set: {"rooms_available": 0}})
db.collections.find({"name": "queen"})

//7
//7
db.collections.deleteMany({"rooms_available": 0})
db.collections.find()
